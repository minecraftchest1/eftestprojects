using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace EntityFrameworkTestInMemory
{
    public class People
    {
        public double ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
    public class Cars
    {
        public double ID { get; }
        public string color { get; }
        public string make { get; }
        public string model { get; }
        //public string manafacturer { get; set; }
        //public double year { get; set; }
        //public People owner { get; set; }
        //public double ownerID { get; set; }
        public double GarageID { get; }

        public Cars(string color, string make, string model, Garage garage)
        {
            this.color = color;
            this.make = make;
            this.model = model;
            this.GarageID = garage.ID;
        }
    }
    public class Garage
    {
        public double ID { get; set; }
        public List<Cars> Cars { get; set; }
    }
    class context : DbContext
    {
        public DbSet<People> People { get; set; }   
        public DbSet<Cars> Cars { get; set; }
        public DbSet<Garage> Garages { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseMySQL(
                "server=192.168.1.40;port=3306;database=test_5_25_2021;user=minecraftchest1;password=vFRUgL1OJ7mw"
            );
    }
}