﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using Minecraftchest1;

namespace EntityFrameworkTestInMemory
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            using (var context = new context())
            {
                Garage MyGarage = new Garage();
                People MyPerson = new People();
                Cars MyCar = new Cars(color: "Dark blue", make: "Toyota", model: "Corola", garage: MyGarage);

                MyPerson.FirstName="Minecraft";
                MyPerson.LastName="Chest1";

                //MyCar.color="Blue";
                //MyCar.make="Toyota";
                //MyCar.model="Corola";
                //MyCar.year=2018;
                //MyCar.owner=MyPerson;
                //MyCar.ownerID=1;

                MyGarage.Cars = new List<Cars>();
                MyGarage.Cars.Add(MyCar);

                context.Add(MyPerson);
                context.Add(MyCar);
                context.Add(MyGarage);

                
                context.SaveChanges();
            }

            Utils.Pause();
        }
    }
}
