using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace MySqlTest
{
    /*
    public class People
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
    public class Cars
    {
        public int ID { get; }
        public string Color { get; }
        public string Make { get; }
        public string Model { get; }
        public double Year { get; }
        public People Owner { get; set; }

        public Cars(string color, string make, string model, People owner)
        {
            this.Color = color;
            this.Make = make;
            this.Model = model;
            this.Owner = owner;
        }
    }*/
    public class Garage
    {
        
        public int Id { get; }
        //public List<Cars> Cars { get; set; }
    
        public Garage()
        {
            Random rnd = new Random();
            int ID = rnd.Next(0,1000);
            using (var DBContext = new context())
            {
                var tmp = DBContext.Garages.Find(ID);
                while (tmp != null)
                {
                    ID = rnd.Next(0,1000);
                    tmp = DBContext.Garages.Find(ID);
                }
                this.Id = ID;
            }
        }
    }
}