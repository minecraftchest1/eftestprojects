﻿using System;

using Minecraftchest1;

namespace MySqlTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Garage MyGarage = new Garage();

            Console.WriteLine("Garage ID: " + MyGarage.Id);

            using (var DBContext = new context())
            {
                DBContext.Add(MyGarage);
            }

            Utils.Pause();
        }
    }
}
